package com.on3software.bidon3.stjohnslutheranschool.silentauction;

import android.app.Application;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.on3software.bidon3.stjohnslutheranschool.silentauction.api.Helper;
import com.on3software.bidon3.stjohnslutheranschool.silentauction.models.Bid;
import com.on3software.bidon3.stjohnslutheranschool.silentauction.models.Item;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import io.fabric.sdk.android.Fabric;

/**
 * Created by robrusher on 2/26/17.
 */

public class AuctionApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        Fabric.with(this, new Crashlytics());

        ParseObject.registerSubclass(Item.class);
        ParseObject.registerSubclass(Bid.class);

        Parse.initialize(new Parse.Configuration.Builder(this)
            .applicationId(Helper.getConfigValue(this, "parse_application_id"))
            .server(Helper.getConfigValue(this, "parse_server"))
            .build()
        );
        ParseUser.enableRevocableSessionInBackground();

        ParseInstallation.getCurrentInstallation().saveInBackground();

        Parse.setLogLevel(Parse.LOG_LEVEL_DEBUG);
        ParseACL defaultACL = new ParseACL();
        defaultACL.setPublicReadAccess(true);
        ParseACL.setDefaultACL(defaultACL,true);

        // Subscribe to a channel
        ParsePush.subscribeInBackground("Bids", new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.i("Parse Push","Push subscribe success");
                } else {
                    Log.i("Parse Push","Push subscribe fail");
                }
            }
        });
    }
}
