package com.on3software.bidon3.stjohnslutheranschool.silentauction.models;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.util.Date;

/**
 * Created by robrusher on 2/26/17.
 */

@ParseClassName("NewBid")
public class Bid extends ParseObject {

    public static final String INITIAL_BIDDER_EMAIL = "";
    public static final String INITIAL_BIDDER_NAME = "";
    public static final String INITIAL_BID_NUMBER = "";

    public long createdAt = 0;

    public Bid() {}

    public static Bid getInitialBid(int price) {
        Bid bid = new Bid();
        bid.setEmail(INITIAL_BIDDER_EMAIL);
        bid.setName(INITIAL_BIDDER_NAME);
        bid.setBidNumber(INITIAL_BID_NUMBER);
        bid.setAmount(price);
        return bid;
    }

    public Date getCreatedAtDate() {
        return getCreatedAt() == null ? new Date(createdAt) : getCreatedAt();
    }

    public int getAmount() {
        return getInt("amt");
    }

    public void setAmount(int amt) {
        put("amt", amt);
    }

    public String getRelatedItemId() {
        return getString("item");
    }

    public void setRelatedItemId(String id) {
        put("item", id);
    }

    public String getEmail() {
        return getString("email");
    }

    public void setEmail(String email) {
        put("email", email);
    }

    public String getName() {
        return getString("name");
    }

    public void setName(String name) {
        put("name", name);
    }

    public String getBidNumber() {
        return getString("bidNumber");
    }

    public void setBidNumber(String bidNumber) {
        put("bidNumber", bidNumber);
    }

}
