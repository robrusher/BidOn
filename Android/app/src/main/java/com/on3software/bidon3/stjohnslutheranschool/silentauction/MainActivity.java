package com.on3software.bidon3.stjohnslutheranschool.silentauction;

import com.parse.ui.ParseLoginDispatchActivity;

public class MainActivity extends ParseLoginDispatchActivity {

    @Override
    protected Class<?> getTargetClass() {
        return ItemListActivity.class;
    }
}
