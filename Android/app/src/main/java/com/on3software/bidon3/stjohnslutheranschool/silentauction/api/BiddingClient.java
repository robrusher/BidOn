package com.on3software.bidon3.stjohnslutheranschool.silentauction.api;

import android.app.Activity;
import android.util.Log;

import com.on3software.bidon3.stjohnslutheranschool.silentauction.models.Bid;
import com.on3software.bidon3.stjohnslutheranschool.silentauction.models.Item;
import com.parse.ParseException;
import com.parse.ParsePush;
import com.parse.ParseUser;
import com.parse.SaveCallback;

/**
 * Created by robrusher on 2/26/17.
 */

public class BiddingClient {

    static DataManager data = DataManager.getInstance();

    public static void placeBid(final Item item, final int amt, final DataManager.BidCallback after, Activity context) {
        ParseUser currentUser = ParseUser.getCurrentUser();

        // Add our bid and confirm that it's the latest and highest bid afterward
        final Bid bid = new Bid();
        bid.setRelatedItemId(item.getObjectId());
        bid.setAmount(amt);
        bid.setName(currentUser.getString("name"));
        bid.setBidNumber(currentUser.getString("bidNumber"));
        bid.setEmail(currentUser.getEmail());
        bid.saveInBackground(new SaveCallback() {
            @Override
            public void done(final ParseException e) {
                ParsePush.subscribeInBackground("a" + item.getObjectId(), new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        Log.i("TEST", "Subscribed.");
                    }
                });

                data.fetchAllItems(new Runnable() {
                    @Override
                    public void run() {
                        if (after != null)
                            after.bidResult(e == null);
                    }
                });
            }
        });
    }
}