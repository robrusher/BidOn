package com.on3software.bidon3.stjohnslutheranschool.silentauction.models;

import android.app.Activity;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created by robrusher on 2/26/17.
 */

@ParseClassName("Item")
public class Item extends ParseObject {

    ParseUser currentUser = ParseUser.getCurrentUser();

    public Item() {}

    public String getItemNumber() {
        return getString("itemNumber");
    }

    public void setItemNumber(String itemNumber) {
        put("itemNumber", itemNumber);
    }

    public String getName() {
        return getString("name");
    }

    public void setName(String name) {
        put("name", name);
    }

    public String getDescription() {
        return getString("description");
    }

    public void setDescription(String description) {
        put("description", description);
    }

    public String getDonorName() {
        return getString("donorName");
    }

    public void setDonorName(String donorName) {
        put("donorName", donorName);
    }

    public String getCategory() {
        return getString("category");
    }

    public void setCategory(String category) {
        put("category", category);
    }

    public String getContact() {
        return getString("contact");
    }

    public void setContact(String contact) { put("contact", contact); }

    public String getImageUrl() {
        return getString("imageUrl");
    }

    public void setImageUrl(String imageUrl) {
        put("imageUrl", imageUrl);
    }

    public String getFairMarketValue() {
        return "Fair Market Value: " + getString("fmv");
    }

    public void setFairMarketValue(String fmv) {
        put("fmv", fmv);
    }

    public int getQty() {
        return getInt("qty");
    }

    public int getPriceIncrement() {
        return getInt("priceIncrement");
    }

    public void setPriceIncrement(int priceIncrement) {
        put("priceIncrement", priceIncrement);
    }

    public int getCurrentPrice() {
        return getInt("price");
    }

    public void setCurrentPrice(int currentPrice) {
        put("currentPrice", currentPrice);
    }

    public List<String> getCurrentWinners() {
        return getListOrEmptyList("currentWinners");
    }

    public int getNumberOfBids() {
        return getInt("numberOfBids");
    }

    public List<String> getAllBidders() {
        return getListOrEmptyList("allBidders");
    }

    public Date getOpenTime() {
        return getDate("openTime");
    }

    public Date getCloseTime() {
        return getDate("closeTime");
    }

    public List<Integer> getAllBids() {
        List<Integer> bids = getListOrEmptyList("currentPrice");
        Collections.sort(bids, new Comparator<Integer>() {
            @Override
            public int compare(Integer lhs, Integer rhs) {
                return rhs - lhs;
            }
        });

        return bids;
    }

    public boolean isOpenForBidding() {
        return !(getOpenTime().after(new Date()) || getCloseTime().before(new Date()));
    }

    public int getCurrentHighestBid() {
        if (getAllBids().size() > 0) {
            return getAllBids().get(0);
        } else {
            Integer startingPrice = (getCurrentPrice() - getPriceIncrement());
            return startingPrice;
        }
    }

    public int[] getLowHighWinningBid() {
        List<Integer> allBids = getAllBids();
        if (allBids.size() > 0) {
            return new int[]{ allBids.get(allBids.size() - 1), allBids.get(0) };
        }
        else {
            Integer startingPrice = (getCurrentPrice() - getPriceIncrement());
            return new int[]{ startingPrice };
        }
    }

    public boolean hasUserBid(Activity context) {
        return getAllBidders().contains(currentUser.getString("bidNumber"));
    }

    public boolean isWinning(Activity context) {
        return getCurrentWinners().contains(currentUser.getString("bidNumber"));
    }

    public int getMyBidWinningIdx(Activity context) {
        return getCurrentWinners().indexOf(currentUser.getString("bidNumber")) + 1;
    }

    public List getListOrEmptyList(String key) {
        List list = getList(key);
        if (list == null)
            return new ArrayList();
        else
            return list;
    }
}
