package com.on3software.bidon3.stjohnslutheranschool.silentauction.api;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.on3software.bidon3.stjohnslutheranschool.silentauction.events.BidsRefreshedEvent;
import com.on3software.bidon3.stjohnslutheranschool.silentauction.models.Bid;
import com.on3software.bidon3.stjohnslutheranschool.silentauction.models.Item;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by robrusher on 2/26/17.
 */

public class DataManager {
    private List<Item> allItems = new ArrayList<Item>();
    static DataManager singletonInstance;
    Handler handler = new Handler();

    public static final int BID_FETCH_INTERVAL = 3000;
    public static final int RETRY_INTERVAL = 3000;

    public static final String QUERY_ALL = "ALL";
    public static final String QUERY_NOBIDS = "NOBIDS";
    public static final String QUERY_MINE = "MINE";

    ParseQuery<Bid> inflightQuery;
    AsyncHttpClient client = new AsyncHttpClient();
    Context context;

    public DataManager() {}

    public static DataManager getInstance() {
        if (singletonInstance == null)
            singletonInstance = new DataManager();

        return singletonInstance;
    }

    public void fetchAllItems() {
        fetchAllItems(new Runnable() {
            @Override
            public void run() {
                EventBus.getDefault().post(new BidsRefreshedEvent());
            }
        });
    }

    public void fetchAllItems(final Runnable after) {
        Log.i("TEST", "Fetching all items.");
        ParseQuery<Item> query = ParseQuery.getQuery(Item.class);
        query.addAscendingOrder("itemNumber");
        query.setLimit(500);
        query.findInBackground(new FindCallback<Item>() {
            @Override
            public void done(List<Item> items, ParseException e) {
                Log.i("TEST", "Items fetched");

                if (items == null) {
                    Toast.makeText(context, "Whoa! Busy auction. Try again in a minute?", Toast.LENGTH_LONG).show();
                }
                else {
                    allItems.clear();
                    allItems.addAll(items);

                    after.run();
                }
            }
        });
    }

    public List<Item> getItemsMatchingQuery(String query, Activity context) {
        if (query.equals(QUERY_ALL)) {
            return allItems;
        }
        else if (query.equals(QUERY_MINE)) {
            ArrayList<Item> mine = new ArrayList<Item>();
            for (Item item : allItems) {
                if (item.hasUserBid(context))
                    mine.add(item);
            }

            return mine;
        }
        else if (query.equals(QUERY_NOBIDS)) {
            ArrayList<Item> noBids = new ArrayList<Item>();
            for (Item item : allItems) {
                if (getBidQuantityForItem(item.getObjectId()) == 0)
                    noBids.add(item);
            }

            return noBids;
        }
        else {
            //ArrayList<String> queryWords = new ArrayList<String>();
            //queryWords.addAll(Arrays.asList(query.split(" ")));

            ArrayList<Item> results = new ArrayList<Item>();
            for (Item item : allItems) {
                //for (String word : queryWords) {

                    if (query.length() > 1 && (
                            containsIgnoreCase(item.getName(),query.toLowerCase()) ||
                            containsIgnoreCase(item.getDescription(),query.toLowerCase()) ||
                            containsIgnoreCase(item.getItemNumber(),query.toLowerCase()) ||
                            containsIgnoreCase(item.getCategory(),query.toLowerCase()))
                        ) {
                        //if( !results.contains(item) ) {
                            results.add(item);
                        //}
                    }
                //}
            }

            return results;
        }
    }

    public static boolean containsIgnoreCase(String src, String what) {
        final int length = what.length();
        if (length == 0)
            return true; // Empty string is contained

        final char firstLo = Character.toLowerCase(what.charAt(0));
        final char firstUp = Character.toUpperCase(what.charAt(0));

        if (src != null) {
            for (int i = src.length() - length; i >= 0; i--) {
                // Quick check before calling the more expensive regionMatches() method:
                final char ch = src.charAt(i);
                if (ch != firstLo && ch != firstUp)
                    continue;

                if (src.regionMatches(true, i, what, 0, length))
                    return true;
            }
        }

        return false;
    }

    public Item getItemForId(String id) {
        for (Item item : allItems) {
            if (item.getObjectId().equals(id))
                return item;
        }

        return null;
    }

    public void beginBidCoverage(Context context) {
        Log.i("TEST", "Beginning coverage...");
        this.context = context;

        if (allItems.size() > 0)
            refreshBids.run();
        else
            fetchAllItems(refreshBids);
    }

    public void refreshBidsNow(final Runnable after) {
        Log.i("TEST", "Refreshing bids...");
        EventBus.getDefault().post(new BidsRefreshedEvent());

        if (after != null)
            after.run();
    }

    private Runnable refreshBids = new Runnable() {
        @Override
        public void run() {

            // Post an emergency retry runnable
            handler.removeCallbacks(retryRefreshBids);
            handler.postDelayed(retryRefreshBids, RETRY_INTERVAL);

            // Don't bother if we don't have the items yet
            if (allItems.size() == 0) {
                handler.removeCallbacks(refreshBids);
                handler.postDelayed(refreshBids, BID_FETCH_INTERVAL);
                return;
            }

            fetchAllItems(new Runnable() {
                @Override
                public void run() {
                    EventBus.getDefault().post(new BidsRefreshedEvent());
                    // handler.postDelayed(refreshBids, BID_FETCH_INTERVAL);
                }
            });
        }
    };

    private Runnable retryRefreshBids = new Runnable() {
        @Override
        public void run() {
            Log.i("TEST", "Retrying...");

            if (inflightQuery != null) {
                Log.i("TEST", "Cancelling inflight query.");
                inflightQuery.cancel();
                inflightQuery = null;
            }

            handler.removeCallbacks(refreshBids);
            handler.removeCallbacks(retryRefreshBids);
            refreshBids.run();
        }
    };

    public int getBidQuantityForItem(String itemId) {
        return getItemForId(itemId).getNumberOfBids();
    }

    public void endBidCoverage() {
        Log.i("TEST", "Ending coverage...");
        handler.removeCallbacks(refreshBids);
    }

    public static abstract class BidCallback {
        public abstract void bidResult(boolean placed);
    }
}
