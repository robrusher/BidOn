package com.on3software.bidon3.stjohnslutheranschool.silentauction.api;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import com.on3software.bidon3.stjohnslutheranschool.silentauction.R;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by robrusher on 2/26/17.
 */

public final class Helper {
    private static final String TAG = "Helper";

    public static String getConfigValue(Context context, String name) {
        Resources resources = context.getResources();

        try {
            InputStream rawResource = resources.openRawResource(R.raw.config);
            Properties properties = new Properties();
            properties.load(rawResource);
            return properties.getProperty(name);
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Unable to find the config file: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "Failed to open config file.");
        }
        return null;
    }
}
