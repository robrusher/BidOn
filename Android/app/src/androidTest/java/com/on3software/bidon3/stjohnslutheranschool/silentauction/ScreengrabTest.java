package com.on3software.bidon3.stjohnslutheranschool.silentauction;


import android.support.test.espresso.ViewInteraction;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import tools.fastlane.screengrab.Screengrab;
import tools.fastlane.screengrab.UiAutomatorScreenshotStrategy;
import tools.fastlane.screengrab.locale.LocaleTestRule;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@LargeTest
@RunWith(AndroidJUnit4.class)
public class ScreengrabTest {

    private String emailAddress;
    private String password;
    private String search;

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @ClassRule
    public static final LocaleTestRule localeTestRule = new LocaleTestRule();

    @Before
    public void initValidString() {
        // Specify a valid string.
        emailAddress = "email@gmail.com";
        password = "12345678";
        search = "tickets";
    }

    @BeforeClass
    public static void beforeAll() {
        Screengrab.setDefaultScreenshotStrategy(new UiAutomatorScreenshotStrategy());
    }

    @Test
    public void test1LoginScreenGrab() {

        ViewInteraction emailField = onView(
                allOf(withId(R.id.login_username_input),
                withParent(withId(R.id.parse_login)),
                isDisplayed()));
        Log.i("######    isDisplayed","Login username input");

        Screengrab.screenshot("Login"); // get snapshot before touching on email field
        Log.i("######    Screenshot","Login screen");

        emailField.perform(replaceText(emailAddress), closeSoftKeyboard());
        Log.i("######    Replace Text", emailAddress);

        onView(allOf(withId(R.id.login_password_input),
            withParent(withId(R.id.parse_login)),
            isDisplayed())).perform(replaceText(password), closeSoftKeyboard());
        Log.i("######    Replace Text", password);

        onView(allOf(withId(R.id.parse_login_button), withText("Log in"),
            withParent(allOf(withId(R.id.parse_buttons),
            withParent(withId(R.id.parse_login)))),
            isDisplayed())).perform(click());
        Log.i("######    Click","Login button");
    }

    @Test
    public void test2AllItemsScreenGrab() {
        onView(allOf(withId(R.id.dashcard),
                withParent(withId(R.id.itemcard_shell)),
                isDisplayed()));
        Log.i("######    isDisplayed","All items cards");

        Screengrab.screenshot("AllItems"); // get screenshot after a card loads
        Log.i("######    Screenshot","AllItems");
    }

    @Test
    public void test3SearchItemsScreenGrab() {
        onView(allOf(withId(R.id.menu_search),
//            withContentDescription("Search"),
                isDisplayed())).perform(click());
        Log.i("######    Click","Search button");

        ViewInteraction searchField = onView(
                allOf(withClassName(is("android.widget.EditText")),
                        withParent(allOf(withId(R.id.custom),
                                withParent(withId(R.id.customPanel)))),
                        isDisplayed())).perform(click());
        Log.i("######    Click","Search query field");

        searchField.perform(replaceText(search), closeSoftKeyboard());
        Log.i("######    Replace Text", search);

        onView(allOf(withId(android.R.id.button1),
                withText("Search"))).perform(scrollTo(),
                click());
        Log.i("######    Click","Search dialog button");

        onView(allOf(withId(R.id.dashcard),
                withParent(withId(R.id.itemcard_shell)),
                isDisplayed()));
        Log.i("######    isDisplayed","Search cards");

        Screengrab.screenshot("Search"); // get screenshot after a card loads
        Log.i("######    Screenshot","Search");

    }

    @Test
    public void test4MyItemsScreenGrab() {
        onView(allOf(withContentDescription("Men Who Cook"),
                withParent(withId(R.id.toolbar)),
                isDisplayed())).perform(click());
        Log.i("######    Click","Navigation menu button");

        onView(allOf(withId(R.id.menu_myitems),
                withText("my items"),
                isDisplayed())).perform(click());
        Log.i("######    Click","My items button");

        onView(allOf(withId(R.id.dashcard),
                withParent(withId(R.id.itemcard_shell)),
                isDisplayed()));
        Log.i("######    isDisplayed","MyItems cards");

        Screengrab.screenshot("MyItems");
        Log.i("######    Screenshot","MyItems");
    }
}
