//
//  AuctionUITests.swift
//  AuctionUITests
//
//  Created by Rob Rusher on 2/23/17.
//  Copyright © 2017 on3software.com. All rights reserved.
//

import XCTest

class AuctionUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        let app = XCUIApplication()
        setupSnapshot(app)
        app.launch()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {

        let app = XCUIApplication()
        snapshot("Login")
        
        let emailAddressTextField = app.textFields["Email Address"]
        emailAddressTextField.tap()
        emailAddressTextField.typeText("rushermollie+2@gmail.com")
        
        let passwordSecureTextField = app.secureTextFields["Password"]
        passwordSecureTextField.tap()
        passwordSecureTextField.typeText("12345678")
        app.buttons["Login"].tap()
        
        snapshot("AllItems")

        let searchSearchField = app.tables.searchFields["Search"]
        searchSearchField.tap()
        searchSearchField.typeText("tickets")

        snapshot("Search")
        
        app.navigationBars["Auction.ItemListView"].buttons["My Bids"].tap()

        snapshot("MyItems")
    }
    
    func waitFor(element:XCUIElement, seconds waitSeconds:Double) {
        let exists = NSPredicate(format: "exists == 1")
        expectation(for: exists, evaluatedWith: element, handler: nil)
        waitForExpectations(timeout: waitSeconds, handler: nil)
    }
}
