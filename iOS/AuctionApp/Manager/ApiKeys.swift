//
//  apiKeys.swift
//  AuctionApp
//

class ApiKeys {
    static let sharedInstance = ApiKeys()
    
    private var apiKeys: NSDictionary!
    
    required init() {
        let filePath = NSBundle.mainBundle().pathForResource("configuration", ofType: "plist")!
        self.apiKeys = NSDictionary(contentsOfFile:filePath)!.objectForKey("APIKeys") as? NSDictionary
    }
    
    var applicationId: String {
        get {
            return apiKeys.objectForKey("ParseApplicationID") as! String
        }
    }
    
    var server: String {
        get {
            return apiKeys.objectForKey("ParseServerURL") as! String
        }
    }
}
