//
//  SignUpViewController.swift
//  AuctionApp
//

import UIKit
import Parse

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var fullnameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func signUpAction(sender: AnyObject) {
        let fullname = self.fullnameField.text
        let password = self.passwordField.text
        let email = self.emailField.text!.lowercaseString
        let finalEmail = email.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        
        // Validate the text fields
        if (fullname?.characters.count) < 5 {
            //let alert = UIAlertView(title: "Invalid", message: "Full Name must be greater than 5 characters", delegate: self, cancelButtonTitle: "OK")
            //alert.show()
            let errorAlertController = UIAlertController(title: "Invalid", message: "Full Name must be greater than 5 characters", preferredStyle: .Alert)
            errorAlertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
            self.presentViewController(errorAlertController, animated: true, completion: nil)
        } else if (password?.characters.count) < 6 {
            //let alert = UIAlertView(title: "Security Requirement", message: "Password must be greater than 6 characters", delegate: self, cancelButtonTitle: "OK")
            //alert.show()
            let errorAlertController = UIAlertController(title: "Security Requirement", message: "Password must be greater than 6 characters", preferredStyle: .Alert)
            errorAlertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
            self.presentViewController(errorAlertController, animated: true, completion: nil)
        } else if (!isValidEmailAddress(email)) {
            //let alert = UIAlertView(title: "Invalid Email", message: "Please enter a valid email address", delegate: self, cancelButtonTitle: "OK")
            //alert.show()
            let errorAlertController = UIAlertController(title: "Invalid Email", message: "Please enter a valid email address", preferredStyle: .Alert)
            errorAlertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
        } else {
            // Run a spinner to show a task in progress
            let spinner: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(0, 0, 150, 150)) as UIActivityIndicatorView
            spinner.startAnimating()
            
            let newUser = PFUser()
            
            newUser["name"] = fullname
            newUser.username = finalEmail
            newUser.email = finalEmail
            newUser.password = password
            
            // Sign up the user asynchronously
            newUser.signUpInBackgroundWithBlock({ (succeed, error) -> Void in
                
                // Stop the spinner
                spinner.stopAnimating()
                if ((error) != nil) {
                    let errorAlertController = UIAlertController(title: "Error", message: "\(error)", preferredStyle: .Alert)
                    errorAlertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                    self.presentViewController(errorAlertController, animated: true, completion: nil)
                    
                } else {
                    let okActionHandler = { (action:UIAlertAction!) -> Void in
                        self.dismissViewControllerAnimated(true, completion: nil)
                    }
                    let successAlertController = UIAlertController(title: "Email Verification", message: "You're almost done! A verification message has been sent to " + finalEmail + ".", preferredStyle: .Alert)
                    successAlertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: okActionHandler))
                    self.presentViewController(successAlertController, animated: true, completion: nil)
                }
            })
        }
    }
    
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try! NSRegularExpression.init(pattern: emailRegEx, options: .CaseInsensitive)
            let nsString = emailAddressString as NSString
            let results = regex.matchesInString(emailAddressString, options: [], range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
        }
        return  returnValue
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
