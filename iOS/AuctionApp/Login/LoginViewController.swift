//
//  LoginViewController.swift
//  AuctionApp
//

import UIKit
import AFViewShaker
import Parse

private var kAssociationKeyNextField: UInt8 = 0

extension UITextField {
    @IBOutlet var nextField: UITextField? {
        get {
            return objc_getAssociatedObject(self, &kAssociationKeyNextField) as? UITextField
        }
        set(newField) {
            objc_setAssociatedObject(self, &kAssociationKeyNextField, newField, .OBJC_ASSOCIATION_RETAIN)
        }
    }
}

class LoginViewController: UIViewController {

    @IBOutlet var usernameTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    var viewShaker:AFViewShaker?
    override func viewDidLoad() {
        super.viewDidLoad()

        viewShaker = AFViewShaker(viewsArray: [usernameTextField, passwordTextField])
        // Do any additional setup after loading the view.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }

    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func textFieldShouldReturn(textField: UITextField) {
        textField.nextField?.becomeFirstResponder()
    }
    
    @IBAction func loginPressed(sender: AnyObject) {
        
        if usernameTextField.text != "" && passwordTextField.text != "" {
            
            let user = PFUser()
            //user["fullname"] = nameTextField.text!.lowercaseString
            user.username = usernameTextField.text!.lowercaseString
            user.password = passwordTextField.text!
            //user.email = emailTextField.text!.lowercaseString
            
            user.signUpInBackgroundWithBlock {
                (succeeded: Bool, error: NSError?) -> Void in
                if succeeded == true {
                    //self.registerForPush()
                    self.performSegueWithIdentifier("loginToItemSegue", sender: nil)
                } else {
                    let errorString = error!.userInfo["error"] as! NSString
                    print("Error Signing up: \(errorString)", terminator: "")
                    PFUser.logInWithUsernameInBackground(user.username!, password: user.password!, block: { (user, error) -> Void in
                        if error == nil {
                            self.registerForPush()
                            self.performSegueWithIdentifier("loginToItemSegue", sender: nil)
                        }else{
                            print("Error logging in ", terminator: "")
                            self.viewShaker?.shake()
                        }
                    })
                }
            }
            
        }else{
            //Can't login with nothing set
            viewShaker?.shake()
        }
    }
    
    @IBAction func loginAction(sender: AnyObject) {
        let username = usernameTextField.text!.lowercaseString
        let password = passwordTextField.text!
        
        // Run a spinner to show a task in progress
        let spinner: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(0, 0, 150, 150)) as UIActivityIndicatorView
        spinner.startAnimating()
            
        // Send a request to login
        PFUser.logInWithUsernameInBackground(username, password: password, block: { (user, error) -> Void in
            
            // Stop the spinner
            spinner.stopAnimating()
                
            if error == nil {
                self.registerForPush()
                self.performSegueWithIdentifier("loginToItemSegue", sender: nil)
            }else{
                print("Error logging in ", terminator: "")
                self.viewShaker?.shake()
            }
        })
    }
    
    func registerForPush() {
        // add user to the installation on parse server
        let user = PFUser.currentUser()!
        let currentInstall = PFInstallation.currentInstallation()
        currentInstall?["user"] = user
        currentInstall?.saveInBackground()
        
        let application = UIApplication.sharedApplication()
        
        if application.respondsToSelector(#selector(UIApplication.registerUserNotificationSettings(_:))) {
            let settings = UIUserNotificationSettings(forTypes: [UIUserNotificationType.Alert, UIUserNotificationType.Sound, UIUserNotificationType.Badge], categories: nil)
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
        }        
    }
    
    @IBAction func unwindToLogInScreen(segue:UIStoryboardSegue){
        
    }
}
