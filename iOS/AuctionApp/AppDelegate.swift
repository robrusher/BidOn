//
//  AppDelegate.swift
//  AuctionApp
//

import UIKit
import Parse
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        // Register for Push notifications
        registerForPushNotifications(application)

        // Register Crashlytics (optional)
        Fabric.with([Crashlytics.self])
        
        // Register customer Parse classes
        Item.registerSubclass()
        Bid.registerSubclass()
        
        // Customize Parse Server configuration
        let configuration = ParseClientConfiguration {
            $0.applicationId = ApiKeys.sharedInstance.applicationId
            $0.clientKey = ""
            $0.server = ApiKeys.sharedInstance.server
        }
        Parse.initializeWithConfiguration(configuration)

        // Required for Parse Server session management
        PFUser.enableRevocableSessionInBackground()
        
        // Determine which screen to go to when the app starts
        let frame = UIScreen.mainScreen().bounds
        window = UIWindow(frame: frame)
        
        let currentUser = PFUser.currentUser()
        if currentUser != nil {
            let itemVC = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle()).instantiateInitialViewController() as? UINavigationController
            window?.rootViewController=itemVC
        } else {
            //Prompt User to Login
            let loginVC = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle()).instantiateViewControllerWithIdentifier("Login") as! LoginViewController
            window?.rootViewController=loginVC
        }
        
        UITextField.appearance().tintColor = UIColor(red: 19/255, green: 43/255, blue: 128/255, alpha: 1.0)

        window?.makeKeyAndVisible()
        
        return true
    }
    
    func registerForPushNotifications(application: UIApplication) {
        let notificationSettings = UIUserNotificationSettings(
            forTypes: [.Badge, .Sound, .Alert], categories: nil)
        application.registerUserNotificationSettings(notificationSettings)
    }

    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {
        if notificationSettings.types != .None {
            application.registerForRemoteNotifications()
        }
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        let currentInstall = PFInstallation.currentInstallation()
        currentInstall?.addUniqueObject("Bids", forKey: "channels")
        
        let tokenChars = UnsafePointer<CChar>(deviceToken.bytes)
        var tokenString = ""
        
        for i in 0 ..< deviceToken.length {
            tokenString += String(format: "%02.2hhx", arguments: [tokenChars[i]])
        }
        
        print("Device tokenString: \(tokenString)", terminator: "")
         
        currentInstall?.setDeviceTokenFromData(deviceToken)
        currentInstall?.saveInBackgroundWithBlock(nil)
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject: AnyObject]) {
        NSNotificationCenter.defaultCenter().postNotificationName("pushReceived", object: userInfo)
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        if error.code == 3010 {
            print("Push notifications are not supported in the iOS Simulator.")
        } else {
            print("application:didFailToRegisterForRemoteNotificationsWithError: %@", error)
        }
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
}
