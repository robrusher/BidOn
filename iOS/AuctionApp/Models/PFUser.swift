//
//  PFUser.swift
//  AuctionApp
//
import Parse

extension PFUser {
    var bidNumber:String?{
        get {
            return self["bidNumber"] as? String
        }
        set {
            self["bidNumber"] = newValue
        }
    }
    var name:String?{
        get {
            return self["name"] as? String
        }
        set {
            self["name"] = newValue
        }
    }
    var paid:Bool?{
        get {
            return self["paid"] as? Bool
        }
        set {
            self["paid"] = newValue
        }
    }
}
