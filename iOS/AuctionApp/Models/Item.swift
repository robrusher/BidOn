//
//  Item.swift
//  AuctionApp
//

//import UIKit
import Parse

enum ItemWinnerType {
    case single
    case multiple
}

class Item: PFObject, PFSubclassing {
    
    @NSManaged var name:String
    @NSManaged var price:Int
    
    var itemNumber:String {
        get {
            if let itemNumberString =  self["itemNumber"] as? String{
                return itemNumberString
            }else{
                return ""
            }
        }
        set {
            self["itemNumber"] = newValue
        }
    }
    
    var itemName:String {
        get {
            if let itemName =  self["name"] as? String{
                return itemName
            }else{
                return ""
            }
        }
        set {
            self["name"] = newValue
        }
    }
    
    var donor:String {
        get {
            if let donor =  self["donorName"] as? String{
                return donor
            }else{
                return ""
            }
        }
        set {
            self["donor"] = newValue
        }
    }
    
    var category:String {
        get {
            if let subtext =  self["category"] as? String{
                return "Category: " + subtext
            }else{
                return ""
            }
        }
        set {
            self["category"] = newValue
        }
    }
    
    var contact:String {
        get {
            if let subtext =  self["contact"] as? String{
                return subtext
            }else{
                return ""
            }
        }
        set {
            self["contact"] = newValue
        }
    }
    
    var fairMarketValue:String {
        get {
            if let fmv = self["fmv"] as? String {
                return "Value: " + fmv
            }else{
                return ""
            }
        }
        set {
            self["fmv"] = newValue
        }
    }
    
    var itemDescription:String {
        get {
            if let desc = self["description"] as? String {
                return desc
            }else{
                return ""
            }
        }
        set {
            self["description"] = newValue
        }
    }
    
    var imageUrl:String {
        get {
            if let imageURLString = self["imageUrl"] as? String {
                return imageURLString
            }else{
                return ""
            }
        }
        set {
            self["imageUrl"] = newValue
        }
    }
    
    var quantity:Int {
        get {
            if let quantityUW =  self["qty"] as? Int{
                return quantityUW
            }else{
                return 0
            }
        }
        set {
            self["qty"] = newValue
        }
    }
    
    var priceIncrement:Int {
        get {
            if let priceIncrementUW = self["priceIncrement"] as? Int {
                return priceIncrementUW
            }else{
                return 5
            }
        }
    }
    
    var currentPrice:[Int] {
        get {
            if let array = self["currentPrice"] as? [Int] {
                return array
            }else{
                return [Int]()
            }
        }
        set {
            self["currentPrice"] = newValue
        }
    }
    
    var currentWinners:[String] {
        get {
            if let array = self["currentWinners"] as? [String] {
                return array
            }else{
                return [String]()
            }
        }
        set {
            self["currentWinners"] = newValue
        }
    }

    var allBidders:[String] {
        get {
            if let array = self["allBidders"] as? [String] {
                return array
            }else{
                return [String]()
            }
        }
        set {
            self["allBidders"] = newValue
        }
    }
    
    var numberOfBids:Int {
        get {
            if let numberOfBidsUW = self["numberOfBids"] as? Int {
                return numberOfBidsUW
            }else{
                return 0
            }
        }
        set {
            self["numberOfBids"] = newValue
        }
    }

    var openTime: NSDate {
        get {
            if let open =  self["openTime"] as? NSDate{
                return open
            }else{
                return NSDate()
            }
        }
    }
    
    var closeTime: NSDate {
        get {
            if let close =  self["closeTime"] as? NSDate{
                return close
            }else{
                return NSDate()
            }
        }
    }
    
    var winnerType: ItemWinnerType {
        get {
            if quantity > 1 {
                return .multiple
            }else{
                return .single
            }
        }
    }

    var minimumBid: Int {
        get {
            if !currentPrice.isEmpty {
                return currentPrice.minElement()!
            }else{
                return price
            }
        }
    }
    
    var isWinning: Bool {
        get {
            let user = PFUser.currentUser()!
            if (user.bidNumber == nil) {
                return false
            }
            return currentWinners.contains(user.bidNumber!)
        }
    }
    
    
    var hasBid: Bool {
        get {
            let user = PFUser.currentUser()!
            if (user.bidNumber == nil) {
                return false
            }
            return allBidders.contains(user.bidNumber!)
        }
    }
    
    /*override class func initialize() {
        var onceToken : dispatch_once_t = 0;
        dispatch_once(&onceToken) {
            self.registerSubclass()
        }
    }*/
    
    class func parseClassName() -> String {
        return "Item"
    }
}


