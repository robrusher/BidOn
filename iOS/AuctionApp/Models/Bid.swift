//
//  Bid.swift
//  AuctionApp
//

import UIKit
import Foundation
import Parse

class Bid: PFObject, PFSubclassing {
    
    @NSManaged var email: String
    @NSManaged var bidNumber: String
    
    var amount: Int {
        get {
            return self["amt"] as! Int
        }
        set {
            self["amt"] = newValue
        }
    }
    
    var itemId: String {
        get {
            return self["item"] as! String
        }
        set {
            self["item"] = newValue
        }
    }
    
    //Needed
    override init(){
        super.init()
    }
    
    init(email: String, bidNumber: String, amount: Int, itemId: String) {
        super.init()
        self.email = email
        self.bidNumber = bidNumber
        self.amount = amount
        self.itemId = itemId
    }
        
    class func parseClassName() -> String {
        return "NewBid"
    }
}

enum BidType {
    case extra(Int)
    case custom(Int)
}
