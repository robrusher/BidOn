//
//  BiddingViewController.swift
//  AuctionApp
//

import UIKit
import IHKeyboardAvoiding

protocol BiddingViewControllerDelegate {
    func biddingViewControllerDidBid(viewController: BiddingViewController, onItem: Item, amount: Int)
    func biddingViewControllerDidCancel(viewController: BiddingViewController)
}

private enum BiddingViewControllerState{
    case custom
    case standard
}

class BiddingViewController: UIViewController {

    @IBOutlet var darkView: UIView!
    @IBOutlet var popUpContainer: UIView!
    @IBOutlet var predifinedButtonsContainerView: UIView!
    @IBOutlet var customBidButton: UIButton!
    @IBOutlet var customBidTextField: UITextField!
    @IBOutlet var plusOneButton: UIButton!
    @IBOutlet var plusTenButton: UIButton!
    @IBOutlet var plusFiveButton: UIButton!
    var delegate: BiddingViewControllerDelegate?
    var item: Item?
    var startPrice = 0
    private var state :BiddingViewControllerState = .standard
    
    var incrementOne = 0
    var incrementFive = 0
    var incrementTen = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customBidTextField.alpha = 0.0
        
        if let itemUW = item{
            
            incrementOne  = itemUW.priceIncrement
            incrementFive = 2*itemUW.priceIncrement
            incrementTen  = 3*itemUW.priceIncrement
            
            if (itemUW.numberOfBids == 0) {
                incrementOne  -= itemUW.priceIncrement
                incrementFive -= itemUW.priceIncrement
                incrementTen  -= itemUW.priceIncrement
            }
            
            switch(itemUW.winnerType){
            case .multiple:
                if itemUW.currentWinners.isEmpty{
                    setupForSingle(itemUW.price)
                }else{
                    setupForSingle(itemUW.currentPrice.last!)
                }
            case .single:
                if itemUW.currentWinners.isEmpty{
                    setupForSingle(itemUW.price)
                }else{
                    setupForSingle(itemUW.currentPrice.first!)
                }
            }
            
            //popUpContainer.layer.cornerRadius = 5.0
            
            IHKeyboardAvoiding.setBuffer(20)
            IHKeyboardAvoiding.setPadding(20)
            IHKeyboardAvoiding.setAvoidingView(view, withTriggerView: popUpContainer)
            
            animateIn()
        }
    }

    @IBAction func didTapBackground(sender: AnyObject) {
        if delegate != nil {
            
            UIView.animateWithDuration(0.2, animations: { () -> Void in
                self.popUpContainer.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01);
                self.darkView.alpha = 0
            }, completion: { (finished: Bool) -> Void in
                self.delegate!.biddingViewControllerDidCancel(self)
            })
            
        }
    }
    func setupForSingle(startAmount: Int) {
        startPrice = startAmount

        let bidAttrs = [NSFontAttributeName : UIFont(name: "Avenir-Light", size: 14.0)! , NSForegroundColorAttributeName: UIColor.grayColor()] as NSDictionary
        let otherAttrs = [NSFontAttributeName : UIFont(name: "Avenir-Light", size: 24.0)!, NSForegroundColorAttributeName: UIColor(red: 19/255, green: 43/255, blue: 128/255, alpha: 1)]
        
        plusOneButton.titleLabel?.textAlignment = .Center
        plusFiveButton.titleLabel?.textAlignment = .Center
        plusTenButton.titleLabel?.textAlignment = .Center

        let one = NSMutableAttributedString(string: "BID\n", attributes: bidAttrs as! [String: NSObject])
        one.appendAttributedString(NSMutableAttributedString(string: "$\(startAmount + incrementOne)", attributes: otherAttrs))
        plusOneButton.setAttributedTitle(one, forState: .Normal)
        
        let five = NSMutableAttributedString(string: "BID\n", attributes: bidAttrs as! [String: NSObject])
        five.appendAttributedString(NSMutableAttributedString(string: "$\(startAmount + incrementFive)", attributes: otherAttrs))
        plusFiveButton.setAttributedTitle(five, forState: .Normal)
        
        let ten = NSMutableAttributedString(string: "BID\n", attributes: bidAttrs as! [String: NSObject])
        ten.appendAttributedString(NSMutableAttributedString(string: "$\(startAmount + incrementTen)", attributes: otherAttrs))
        plusTenButton.setAttributedTitle(ten, forState: .Normal)
    }
    
    func setupForMultiple() {
        self.customBidTextField.alpha = 1.0
        self.predifinedButtonsContainerView.alpha = 0.0
        self.customBidButton.setTitle("Bid", forState: .Normal)
        state = .custom
    }

    func didSelectAmount(bidType: BidType) {
        
        var amount = 0
        switch bidType {
        case .custom(let total):
            amount = total
        case .extra(let aditional):
            amount = startPrice + aditional
        }
        
        if delegate != nil {
            if let itemUW = item {
                
                UIView.animateWithDuration(0.2, animations: { () -> Void in
                    self.popUpContainer.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01);
                    self.darkView.alpha = 0
                }, completion: { (finished: Bool) -> Void in
                    self.delegate!.biddingViewControllerDidBid(self, onItem: itemUW, amount: amount)
                })
                
                
            }
        }
    }
    
    @IBAction func customAmountPressed(sender: AnyObject) {
        
        switch state {
        case .custom:
            if let amount = Int(customBidTextField.text!){
                didSelectAmount(.custom(amount))
            }else{
                didTapBackground("" as AnyObject)
            }
        case .standard:
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.setupForMultiple()
                self.customBidTextField.becomeFirstResponder()
            })
        }
    }

    @IBAction func bidOneDollarPressed(sender: AnyObject) {
        didSelectAmount(.extra(incrementOne))
    }

    @IBAction func bidFiveDollarPressed(sender: AnyObject) {
        didSelectAmount(.extra(incrementFive))
    }
    
    @IBAction func bidTenDollarPressed(sender: AnyObject) {
        didSelectAmount(.extra(incrementTen))
    }
    
    func animateIn(){
        popUpContainer.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01);
        
        UIView.animateWithDuration(0.5,
            delay: 0.0,
            usingSpringWithDamping: 0.6,
            initialSpringVelocity: 0.0,
            options: UIViewAnimationOptions.CurveLinear,
            animations: {
                self.popUpContainer.transform = CGAffineTransformIdentity
                self.darkView.alpha = 1.0
                
            },
            completion: { (fininshed: Bool) -> () in

            }
        )
    }
}
