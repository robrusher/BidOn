BidOn3 iOS
==============
iOS client for On3 Software's open-source silent auction app. For an overview of the silent auction app project, [check out our post about it](http://on3software.com/)!

<img src="http://i.imgur.com/9ihghFu.png" width="200">
<img src="http://i.imgur.com/saJFGFz.png" width="200">
<img src="http://i.imgur.com/XGp9TeU.png" width="200">
<img src="http://i.imgur.com/akbZrEF.png" width="200">

## Getting started
If you haven't yet, you're going to want to set up Parse Server by following the instructions in the [BidOn3 Cloud Code repository](https://github.com/on3software/BidOn3-CloudCode). Make a note of your application ID and server URL. All set?

This project uses CocoaPods for dependency management. If you don't have Cocoapods, install it first: `sudo gem install cocoapods`.

Then, `git clone` this repository. From the AuctionApp directory, run `pod install`, which will tell CocoaPods to grab all of the app's dependencies. Then, just open `AuctionApp.xcworkspace` (not `AuctionApp.xcodeproj` - this will break CocoaPods) in Xcode. 

Edit *AuctionApp/configuration.example.plist*, replacing `MY PARSE APPLICATION ID` and `MY PARSE SERVER URL` with the application ID and server URL you copied from Parse Server. Then rename the file name to `configuration.plist`. Run the app and you should be all set... almost! Try bidding on something. To keep an eye on the action, check out the [Web Panel](https://github.com/On3Software/BidOn3-WebMonitor) where you can see all your items and bids.

Push isn't going to work yet, but you should be able to see Test Object 7 and bid on it. If you have the Android app up and running, and already bid on the Test Object, your Android phone will receive a sassy push notification.

## Customization

This is built to be customized:
* `banner` is the login banner
* `AppIcon` is the app icon
* `LaunchImage` is the app splash screen

## Push
Setting up push for iOS devices takes a bit of work. It's an ordeal, but a manageable ordeal, and it's been [well documented by Parse](https://parseplatform.github.io/docs/ios/guide/#push-notifications.
