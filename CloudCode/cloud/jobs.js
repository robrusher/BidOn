// Sets up all the tables for you.
Parse.Cloud.job("InitializeForAuction", function(request, status) {

	// Add a test item.
	var Item = Parse.Object.extend("Item"); 
	var item = new Item();  

	// common auction item information
	item.set("itemNumber", "0");
	item.set("name", "Name of item");
	item.set("donorName", "Item donor");
	item.set("category", "Category");
	item.set("contact", "Contact URL or Phone number");
	item.set("fmv", "25");
	item.set("description", "This is a test object, and you (probably) won't be asked to donate your bid on this item to charity. Who knows, though.");
	item.set("imageUrl", "http://i.imgur.com/kCtWFwr.png");
	item.set("qty", 3);
	item.set("price", 50);
    item.set("priceIncrement", 5);
    item.set("itemType","silent");

    // columns used to manage the item during the auction
	item.set("currentPrice", []);
	item.set("currentWinners", []);
	item.set("allBidders", []);
	item.set("numberOfBids", 0);
	item.set("openTime", new Date("Mar 11, 2017, 09:00"));
	item.set("closeTime", new Date("Mar 11, 2017, 22:00"));
	item.set("previousWinners", []);
console.log("Item initialization complete.");

	// save new item
	item.save(null, {
		success: function(item) {
console.log("Begin NewBid "+item.objectId);
			var NewBid = Parse.Object.extend("NewBid"); 
			var bid = new NewBid();  
			bid.set("item", item.objectId);
			bid.set("amt", 0);
			bid.set("email", "robrusher@gmail.com");
			bid.set("name", "Rob Rusher");
			bid.set("bidNumber","100")
			bid.save(null, {
				success: function(bid) {
					console.log("NewBid initialization complete.");
				},
				error: function(bid) {
					console.log("NewBid failed to initialize.");
				}
			});
		},
		error: function(item, error) {
			console.error("Unable to initialize Item table. Have you set your application name, app ID, and master key in config/global.json?")
		}
	});
});
