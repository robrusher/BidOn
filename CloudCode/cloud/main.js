// Utility to get items unique to either array.
Array.prototype.diff = function(a) {
	return this.filter(function(i) {return a.indexOf(i) < 0;});
};

Parse.Cloud.beforeSave(Parse.User, function(request, response) {
    // Check if the User is newly created
    if (request.object.isNew()) {
        // Set default values
        request.object.set("paid", false);
        request.object.set("amountPaid", 0);
        request.object.set("paymentType", "");
        request.object.set("comments", "");
    }
    response.success();
});

//Parse.Cloud.afterSave(_User, function(request, response){
//	var installationQuery = new Parse.Query(Parse.Installation);
//	installationQuery.find(request.object.email,)
//});

// This code will be run before saving a new bid.
Parse.Cloud.beforeSave("NewBid", function(request, response) {

	currentBid = request.object;

	// Grab the item that is being bid on.
	itemQuery = new Parse.Query("Item");
	itemQuery.equalTo("objectId", request.object.get("item"));
	itemQuery.first({
		success: function(item) {

			if (!item)
			 return;

			var date = Date.now();

			// Make sure bidding on this item hasn't closed
			if (date > item.get("closeTime")) {
				response.error("Bidding for this item is closed");
				return;
			}

			// Make sure the bid is not lower than the starting bid
			if (currentBid.get("amt") < item.get("price")) {
				response.error("The starting bid is " + item.get("price"));
				return;
			}

            minIncrement = item.get("priceIncrement");
            itemsRemaining = item.get("currentWinners").length % item.get("qty");
            
            if (itemsRemaining) { // if itemsRemaining, you must at least match the current lowest bid
				if (currentBid.get("amt") < (Number(item.get("currentPrice")))) {
					response.error("You need to beat the lowest bid in order to win 1 of " + item.get("qty"));
					return;
				}
            } else {
				// if the bid increments by at least the minimum increment
				if (currentBid.get("amt") < (Number(item.get("currentPrice")) + minIncrement )) {
					response.error("You need to raise the bid by at least $" + minIncrement);
					return;
				}
			}

			// Sanity check. In-house testing revealed that people love bidding one trillion dollars.
			if (currentBid.get("amt") > 99999) {
				response.error("Whoa big spender! Is that a typo?");
				return;
			}

			// Retrieve all previous bids on this item.
			query = new Parse.Query("NewBid");
			query.equalTo("item", request.object.get("item"));
			query.descending("amt", "createdAt");
			query.limit = 1000; // Default is 100
			query.find({
			  success: function(allWinningBids) {

			  	item.set("numberOfBids", allWinningBids.length + 1);

			    var quantity = item.get("qty");
			    var currentPrice = [];
			    var currentWinners = [];
				var previousWinners = item.get("currentWinners");

   			    var allBidders = item.get("allBidders");
   			    if (!allBidders) {
   			    	allBidders = [];
   			    }

			    // Build an object mapping bidNumbers to their highest bids.
   			    var bidsForNumber = {};
   			    allWinningBids.forEach(function(bid) {
			   		var curBid = bidsForNumber[bid.get("bidNumber")]
			   		if (curBid) {
		   				bidsForNumber[bid.get("bidNumber")] = (curBid.get("amt") > bid.get("amt") ? curBid : bid);
			   		}
			   		else {
			   			bidsForNumber[bid.get("bidNumber")] = bid;
			   		}
   			    });

				// Get this bidder's last bid and make sure the new bid is an increase.
			  	// If the new bid is higher, remove the old bid.
   			    var previousMaxBid = bidsForNumber[currentBid.get("bidNumber")];
   			    if (previousMaxBid) {
   			    	if (currentBid.get("amt") <= previousMaxBid.get("amt")){
			    		response.error("You already bid $" + previousMaxBid.get("amt") + " - you need to raise your bid!");
		    			return;
   			    	}
					else {
   			    		delete bidsForNumber[currentBid.get("bidNumber")];
   			    	}
   			    }

				// Build an array of all the winning bids.
   			    allWinningBids = [];
   			    for (var key in bidsForNumber) {
   			    	allWinningBids.push(bidsForNumber[key]);
   			    }

			  	// Add the new bid and sort by amount, secondarily sorting by time.
   			    allWinningBids.push(currentBid)
   			    allWinningBids = allWinningBids.sort(function(a, b){
    					var keyA = a.get("amt");
    					var keyB = b.get("amt");

					    // Sort on amount if they're different.
					    if (keyA < keyB) {
					    	return 1;
					    }
					    else if (keyA > keyB) {
					     	return -1;
					 	}

    					var dateKeyA = a.get("createdAt");
    					var dateKeyB = b.get("createdAt");

						// Secondarily sort on time if the amounts are the same.
					    if (dateKeyA < dateKeyB) {
					    	return 1;
					    }
					    else if (dateKeyA > dateKeyB) {
					     	return -1;
					 	}

					    return 0;
					});

				// Slice off either the top n bids (for an item where the highest n bids win)
			  	// or all of them if there are fewer than n bids.
   			    var endIndex = 0;
   			    if (quantity > allWinningBids.length) {
   			    	endIndex = allWinningBids.length;
   			    }
				else {
   			    	endIndex = quantity;
   			    }

				var newBidIsWinning = false;
		    	var currentWinningBids = allWinningBids.slice(0, endIndex);

		    	// If the new bid is in the list of winning bids...
		    	if (currentWinningBids.indexOf(currentBid) != -1){
		    		newBidIsWinning = true;

		    		// if the item is limited to 1 bider (i.e. cork pull), close the item
		    		var itemType = item.get("itemType");
		    		if (itemType == "limit_1") {
		    			item.set("closeTime", new Date());
		    		}

					// Update the item's current price and current winners.
					for (var i = 0; (i < currentWinningBids.length); i++) {
						var bid = currentWinningBids[i];
						currentPrice.push(bid.get("amt"));
						currentWinners.push(bid.get("bidNumber"));
					}

			    	// Add this bidder to the list of all bidders...
			    	allBidders.push(currentBid.get("bidNumber"));

					// ...and remove them if they're already there.
			    	var uniqueArray = allBidders.filter(function(elem, pos) {
						return allBidders.indexOf(elem) == pos;
					});

				    item.set("allBidders", uniqueArray);
					item.set("currentPrice", currentPrice);
					item.set("currentWinners", currentWinners);
					item.set("previousWinners", previousWinners)

					// Save all these updates back to the Item.
					item.save(null, {
						success: function(item) {
							response.success();
						},
						error: function(item, error) {
							console.error(error);
							response.error("Something went wrong while saving the bid.");
						}
					});
		    	}
				// If it's not, someone else probably outbid you in the meantime.
		    	else {
			    	response.error("You've got move faster than that! Someone beat you to it.\nCheck the new price and try again.");
		    		return;
		    	}


			  },
			  error: function(error) {
			    console.error("Error: " + error.code + " " + error.message);
			    response.error("Error: " + error.code + " " + error.message);
			  }
			});

		},
		error: function(error) {
		    console.error("Error: " + error.code + " " + error.message);
		    response.error("Error: " + error.code + " " + error.message);
		}
	});
	
});

// This code is run after the successful save of a new bid.
Parse.Cloud.afterSave("NewBid", function(request, response) {

	currentBid = request.object;

	// Get the item that's being bid on.
	itemQuery = new Parse.Query("Item");
	itemQuery.equalTo("objectId", request.object.get("item"))
	itemQuery.first({
		success: function(item) {

			var previousWinners = item.get("previousWinners");

			// For multi-quantity items, don't bother the people "higher" than you
			// ex: don't send a push to the person with the #1 bid if someone overtakes
			// the #2 person.
			var index = previousWinners.indexOf(currentBid.get("bidNumber"));
			if (index > -1) {
				previousWinners.splice(index, 1);
			}

			// Grab installations where that user was previously a winner but no longer is.
			var userQuery = new Parse.Query(Parse.User);
			userQuery.containedIn("bidNumber", previousWinners.diff(item.get("currentWinners")));

			// Find devices associated with these users
			var pushQuery = new Parse.Query(Parse.Installation);
			pushQuery.matchesQuery("user", userQuery);
			//pushQuery.exists("deviceToken");

			// We'll refer to the bidder by their name if it's set...
			//var identity = currentBid.get("name").split(" ")[0];
			var identity = "Your rival";

			// ...and their email prefix if it's not.
			//if (identity.length < 3) {
			//	identity = currentBid.get("email").split("@")[0];
			//}

			// Fire the push.
			Parse.Push.send({
			  where: pushQuery,
			  data: {
			    alert: identity + " bid $" + currentBid.get("amt") + " on " + item.get("name") + ". Surely you won't stand for this.",
			    itemname: item.get("name"),
			    personname: identity,
			    itemid: item.id,
			    sound: "default",
			    amt: currentBid.get("amt"),
			    email: currentBid.get("email")
			  }
			}, {
			  useMasterKey: true,
			  success: function() {
			    console.log("Pushed successfully: " + identity + " bid $" + currentBid.get("amt") + " on " + item.get("name"))
			  },
			  error: function(error) {
			    console.error("Push failed: " +error)
			  }
			});

		}, 
		error: function(error) {
		    console.error("Push failed: " +error)
		}
	});

});


