var auctionApp = angular.module('auctionApp', []);

auctionApp.controller('ItemCardsCtrl', function ($scope) {

	$scope.userClaims = [];

	$.ajax({
        url: "<your parse server url>",
        type: "GET",
		data: "limit=500",
        beforeSend: function(xhr){
            xhr.setRequestHeader('X-Parse-Application-Id', '<your parse app id>');
        },
        success: function(result) {
            $scope.$apply(function(){
                $scope.items = result.results;

                var totalRaised = 0;
                var totalStartPrice = 0;
                var numBids = 0;
                var topBidCount = "";
                var topBidCountCur = 0;
                var topBidAmt = "";
                var topBidAmtCur = 0;
                var highestGrossing = "";
                var highestGrossingCur = 0;

                var noBidCount = 0;

                $scope.items.forEach(function(item) {
                    var gross = 0;
                    item.currentPrice.forEach(function(bidprice) {
                        totalRaised = totalRaised + bidprice;
                        gross = gross + bidprice;
                    });

                    if (item.currentPrice.length == 0) {
                        noBidCount = noBidCount + 1;
                    }

                    numBids = numBids + item.numberOfBids;

                    if (item.numberOfBids > topBidCountCur) {
                        topBidCount = item.numberOfBids + " Bids - " + item.name + " by " + item.donorName;
                        topBidCountCur = item.numberOfBids;
                    }

                    if (item.currentPrice[0] > topBidAmtCur) {
                        topBidAmt = "$" + item.currentPrice[0] + ": " + item.name + " by " + item.donorName;
                        topBidAmtCur = item.currentPrice[0];
                    }

                    if (gross > highestGrossingCur) {
                        highestGrossingCur = gross;
                        highestGrossing = "$" + gross + ": " + item.name + " by " + item.donorName;
                    }

                    item.gross = gross;

                    item.bidObjs = [];
                    for (var i = 0; i < item.currentWinners.length; i++) {
						var bidNumber = item.currentWinners[i];
                        item.bidObjs.push({"who": bidNumber, "amt": item.currentPrice[i]});

						var newItem = {"item": item.itemNumber, "name": item.name, "amt": item.currentPrice[i]};
						var notFound = true;
						var len = $scope.userClaims.length;

						for (var k = 0; k < len; k++){
							if ( $scope.userClaims[k].bidder == bidNumber ) {
								$scope.userClaims[k].items.push(newItem);
								$scope.userClaims[k].total += newItem.amt;
								notFound = false;
							}
						}

						if ( notFound ) {
							$scope.userClaims[k] = {'bidder':bidNumber, items:[newItem], total:newItem.amt};
						}

						//console.log(JSON.stringify($scope.userClaims));
                    }
                });

                $scope.totalRaised = totalRaised;
                $scope.bidCount = numBids;
                $scope.mostPopularBidCount = topBidCount;

                $scope.mostPopularPrice = topBidAmt;

                $scope.highestGrossing = highestGrossing;
                $scope.itemCount = $scope.items.length;
                $scope.noBidCount = noBidCount;
            });

        },
		error: function(obj,error) {
			console.log( error.message );
		}
    });

    $scope.buildPDF = function() {

		var columns = [
			{title: "Item Number", dataKey: "item"},
			{title: "Item Name", dataKey: "name"},
			{title: "Bid Amount", dataKey: "amt"},
		];

		var doc = new jsPDF('p', 'pt');

		var len = $scope.userClaims.length;
		for (var j = 0; j < len; j++) {
			var pageContent = function (data) {
				doc.setFontSize(20);
				doc.setTextColor(40);
				doc.setFontStyle('bold');
				doc.text("Claim Sheet for Bidder # "+ $scope.userClaims[j].bidder, data.settings.margin.left, 50);
			}
			//console.log("User Claims: " + JSON.stringify($scope.userClaims[j]) );

			doc.autoTable(columns, $scope.userClaims[j].items, {
				addPageContent: pageContent,
				pageBreak: 'avoid',
				margin: {top: 60},
				columnStyles: {text: {columnWidth: 'auto'}}
			});

			var str = "Total: $" + $scope.userClaims[j].total;
			doc.text(str, 400, doc.autoTable.previous.finalY + 20);

			if(j+1 < len)
				doc.addPage();
		}

		doc.save('table.pdf');
    };

});

auctionApp.filter('orderObjectBy', function(){
    return function(input, attribute) {
        if (!angular.isObject(input)) return input;

        var array = [];
        for(var objectKey in input) {
            array.push(input[objectKey]);
        }

        array.sort(function(a, b){
            a = parseInt(a[attribute]);
            b = parseInt(b[attribute]);
            return b - a;
        });
        return array;
    }
});

auctionApp.filter('noFractionCurrency',
    [ '$filter', '$locale',
        function(filter, locale) {
            var currencyFilter = filter('currency');
            var formats = locale.NUMBER_FORMATS;
            return function(amount, currencySymbol) {
                var value = currencyFilter(amount, currencySymbol);
                var sep = value.indexOf(formats.DECIMAL_SEP);
                if(amount >= 0) {
                    return value.substring(0, sep);
                }
                return value.substring(0, sep) + ')';
            };
        } ]);
