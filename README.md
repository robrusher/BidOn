# BidOn3
Open source silent auction platform with native iOS and Android app, built on the Open Source [Parse Server](https://parseplatform.github.io/).

This project was based on the [BidHub](http://www.bidhub.org/) project by HubSpot.
